﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Tests
{
    [TestClass()]
    public class GeneralTests
    {
        [TestMethod()]
        public void freqIntTest()
        {
            int[] nums = { 1, 5, 2, 6, 1, 4, 6, 1 };
            Assert.AreEqual(1, General.freqInt(nums));
        }

        [TestMethod]
        public void isRotatedTest()
        {
            int[] nums = { 1, 2, 3, 5, 6, 7, 8 };
            int[] rot = { 5, 6, 7, 8, 1, 2, 3 };
            Assert.IsTrue(General.isRotated(nums, rot));
        }

        [TestMethod()]
        [Description("Just prints to console")]
        public void sumPairsTest()
        {
            int[] nums = { 1,  4, 3, 6, 9 };
            int[] expected = { 1, 4 };
            Assert.Fail();
        }

        [TestMethod]
        public void isRotatedFailTest()
        {
            int[] nums = { 1, 2, 3, 5, 6, 7, 8 };
            int[] rot = { 5, 6, 100, 8, 1, 2, 3 };
            Assert.IsFalse(General.isRotated(nums, rot));
        }

        [TestMethod]
        public void iterFibTest()
        {
            Assert.AreEqual(13, General.iterFib(7));
        }

        [TestMethod]
        public void curseFibTest()
        {
            Assert.AreEqual(6765, General.curseFib(20));
        }

        [TestMethod]
        public void occurOnce()
        {
            int[] nums = { 1, 1, 2, 2, 3, 4, 4, 5, 5, 5 };
            Assert.AreEqual(3, General.occurOnce(nums));
        }

        [TestMethod]
        public void commonElementsTest()
        {
            int[] a = { 1, 2, 3, 4, 5, 6 };
            int[] b = { 5, 7, 8, 9, 10 };
            int[] expected = { 5 };
            CollectionAssert.AreEqual(expected, General.commonElements(a, b).ToList());
        }

        [TestMethod]
        public void binSearchTest()
        {
            int[] a = { 1, 2, 3, 4, 5, 6, 7 };
            int search = 3;
            Assert.AreEqual(2, General.binSearch(a, search));
        }

        [TestMethod]
        public void rotBinSearchTest()
        {
            int[] a = { 5, 6, 7, 8, 1, 2, 3 };
            int search = 7;
            Assert.AreEqual(2, General.rotBinSearch(a, search));
        }

        [TestMethod]
        public void primeNumsTest()
        {
            int[] expected = { 2, 3, 5, 7, 11, 13 };
            CollectionAssert.AreEqual(expected, General.primeNums(6).ToArray());

        }

        [TestMethod]
        public void binIntTest()
        {
            string expected = "000000001";
            StringAssert.Equals(expected, General.intBinConverter(1));
        }
    }
}