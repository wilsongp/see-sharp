﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Tests
{
    [TestClass()]
    public class SortsTests
    {
        [TestMethod()]
        public void bubbleTest()
        {
            int[] nums = { 4, 6, 2, 7, 8, 3, 5 };
            int[] expected = { 2, 3, 4, 5, 6, 7, 8 };
            CollectionAssert.AreEqual(expected, Sorts.bubble(nums).ToArray());
        }

        [TestMethod()]
        public void selectionTest()
        {
            int[] nums = { 4, 6, 2, 7, 8, 3, 5 };
            int[] expected = { 2, 3, 4, 5, 6, 7, 8 };
            CollectionAssert.AreEqual(expected, Sorts.selection(nums).ToArray());
        }

        [TestMethod()]
        public void insertionTest()
        {
            int[] nums = { 4, 6, 2, 7, 8, 3, 5 };
            int[] expected = { 2, 3, 4, 5, 6, 7, 8 };
            CollectionAssert.AreEqual(expected, Sorts.insertion(nums).ToArray());
        }

        [TestMethod()]
        public void mergeTest()
        {
            int[] nums = { 4, 6, 2, 7, 8, 3, 5 };
            int[] expected = { 2, 3, 4, 5, 6, 7, 8 };
            CollectionAssert.AreEqual(expected, Sorts.merge(nums).ToArray());
        }

        [TestMethod()]
        public void quickTest()
        {
            int[] nums = { 4, 6, 2, 7, 8, 3, 5 };
            int[] expected = { 2, 3, 4, 5, 6, 7, 8 };
            CollectionAssert.AreEqual(expected, Sorts.quick(nums).ToArray());
        }
    }
}