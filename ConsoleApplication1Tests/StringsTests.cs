﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Tests
{
    [TestClass()]
    public class StringsTests
    {
        [TestMethod()]
        public void nonRepeatTest()
        {
            string test = "aabbccdeeffg";
            Assert.AreEqual('d', Strings.nonRepeat(test));
        }

        [TestMethod()]
        public void reverseIterTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void reverseCurseTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void isAnagramTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void isPalindromeTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void uniqueCharsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void intOrDoubleTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void shortestPalindromeTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void allPermutationsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void justifyTest()
        {
            Assert.Fail();
        }
    }
}