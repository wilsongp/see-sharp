﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Node<T>
    {
        public Node<T> leftNode;
        public Node<T> rightNode;
        public T data;
        public Node(T data)
        {
            this.leftNode = null;
            this.rightNode = null;
            this.data = data;
        }
    }
}
