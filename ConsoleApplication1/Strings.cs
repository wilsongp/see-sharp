﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public static class Strings
    {
        /**
         * Find the first non-repeated character in a String
         */
        public static char nonRepeat(string str)
        {
            return str.ToArray()
                .GroupBy(x => x)
                .FirstOrDefault(x => x.Count() == 1).First();
        }

        /**
         * Reverse a String iteratively and recursively
         */
        public static char reverseIter(string str)
        {
            throw new NotImplementedException();
        }
        public static char reverseCurse(string str)
        {
            throw new NotImplementedException();
        }

        /**
         * Determine if 2 Strings are anagrams
         */
        public static bool isAnagram(string a, string b)
        {
            throw new NotImplementedException();
        }

        /**
         * Check if String is a palindrome
         */
        public static bool isPalindrome(string str)
        {
            throw new NotImplementedException();
        }

        /**
         * Check if a String is composed of all unique characters
         */
        public static bool uniqueChars(string str)
        {
            throw new NotImplementedException();
        }

        /**
         * Determine if a String is an int or a double
         */
        public static bool intOrDouble(string str)
        {
            throw new NotImplementedException();
        }

        /**
         * HARD: Find the shortest palindrome in a String
         */
        public static string shortestPalindrome(string str)
        {
            throw new NotImplementedException();
        }

        /**
         * HARD: Print all permutations of a String
         */
        public static void allPermutations(string str)
        {
            throw new NotImplementedException();
        }

        /**
         * HARD: Given a single-line text String and a maximum width value, write the function 
         *  'String justify(String text, int maxWidth)' that formats the input text using full-justification, 
         *  i.e., extra spaces on each line are equally distributed between the words; the first word on 
         *  each line is flushed left and the last word on each line is flushed right
         */
        public static string justify(string str)
        {
            throw new NotImplementedException();
        }
    }
}
