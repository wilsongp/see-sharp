﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Trees
    {
        
        /**
         * Write a function that determines if a tree is a IList<int>
         */
        public static bool isBST(IList<int> tree)
        {
            throw new NotImplementedException();
        }

        /**
         * Given a binary tree which is a sum tree (child nodes add to parent), 
         * write an algorithm to determine whether the tree is a valid sum tree
         */
        public static bool isValidSumBT(IList<int> tree)
        {
            throw new NotImplementedException();
        }

        /**
         * Find the distance between 2 nodes in a IList<int> and a normal binary tree
         */
        public static int distanceBST(int a, int b)
        {
            throw new NotImplementedException();
        }
        public static int distanceBT(int a, int b)
        {
            throw new NotImplementedException();
        }

        /**
         * Given a tree, verify that it contains a subtree.
         */
        public static bool hasSubtree(IList<int> tree)
        {
            throw new NotImplementedException();
        }

        /**
         * HARD: Find the max distance between 2 nodes in a IList<int>.
         */
        public static int maxDistance(int a, int b)
        {
            throw new NotImplementedException();
        }

        /**
         * HARD: Construct a IList<int> given the pre-order and in-order traversal Strings
         */
        public static bool preInOrderTree(string a, string b)
        {
            throw new NotImplementedException();
        }
    }
}
