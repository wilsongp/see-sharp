﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public static class General
    {
        /**
         * Find the most frequent integer in an array
         *  OPTIMIZE
         **/
        public static int freqInt(int[] nums)
        {
            Dictionary<int, int> counts = new Dictionary<int, int>();
            for (int i = 0; i < nums.Length; i++)
            {
                if (counts.ContainsKey(nums[i]))
                {
                    counts[nums[i]]++;
                }
                else
                {
                    counts.Add(nums[i], 1);
                }
            }

            return counts.FirstOrDefault(x => x.Value == counts.Values.Max()).Key;
        }

        /**
         * Find pairs in an integer array whose sum is equal to 10 (bonus: do it in linear time)
         **/
        public static void sumPairs(IList<int> nums)
        {
            // pairs = 2 ints
            // loop over array and insert value with key of compliment
            IDictionary<int, int> check = new Dictionary<int, int>(); 
            for (int i = 0; i < nums.Count; i++)
            {
                // store compliment to check against
                int key = 10 - nums[i];
                check[nums[i]] = i;
                // if the key exists and not same index then the compliment has been seen so it's a match!
                if (check.ContainsKey(key) && check[key] != i)
                {
                    Console.WriteLine("{0} + {1} = {2} | index: {3}", nums[i], key, key + nums[i], i);
                }
            }
        }

        /**
         * Given 2 integer arrays, determine if the 2nd array is a rotated version of the 1st array. 
         * Ex. Original Array A={1,2,3,5,6,7,8} Rotated Array B={5,6,7,8,1,2,3}
         * BIGO: O(n)
         */
        public static bool isRotated(IList<int> a, IList<int> b)
        {
            //Check lengths
            if (a.Count != b.Count)
            {
                return false;
            }

            // Get position of first in compare array
            int offset = b.IndexOf(a.First());
            if (offset < 0)
            {
                return false;
            }
            for (int i = 0; i < a.Count; i++)
            {
                int relIndex = (i + offset) % a.Count;
                if (a[i] != b[relIndex])
                {
                    return false;
                }
            }
            return true;
        }

        /**
         * Iterative fibbonacci
         */
        public static int iterFib(int n)
        {
            int prev = 0;
            int ret = 1;
            for (int i = ret; i < n; i++)
            {
                int tmp = ret;
                ret = prev + ret;
                prev = tmp;
            }
            return ret;
        }

        /**
         * recursive fibbonacci
         */
        private static Dictionary<int,int> memo = new Dictionary<int, int>();
        public static int curseFib(int n)
        {
            if (n <= 1)
            {
                return n;
            }
            if (memo.ContainsKey(n))
            {
                return memo[n];
            }

            int res = curseFib(n - 1) + curseFib(n - 2);
            memo[n] = res;
            return res;
        }

        /**
         * Find the only element in an array that only occurs once.
         */
        public static int occurOnce(IList<int> nums)
        {
            IEnumerable<int> singles = nums
                .GroupBy(x => x)
                .Where(x => x.Count() == 1)
                .Select(x => x.First());

            if (singles.Count() > 1)
            {
                throw new Exception("Need ONE int to appear once");
            }
            return singles.First();
        }

        /**
         * Find the common elements of 2 int arrays
         * Enumerable.intersect() is O(m+n)
         * **** Actually implement
         */
        public static IEnumerable<int> commonElements(IList<int> a, IList<int> b)
        {
            return a.Intersect(b);
        }

        /**
         * Implement binary search of a sorted array of integers
         */
        public static int binSearch(IList<int> sorted, int search)
        {
            int low = 0;
            int high = sorted.Count - 1;
            while (low < high)
            {
                int pivot = low + (int)((high - low) / 2);
                if (sorted[pivot] == search)
                {
                    return pivot;
                }
                if (search < sorted[pivot])
                {
                    high = pivot;
                }
                else
                {
                    low = pivot;
                }
            }
            return -1;
        }

        /**
         * Implement binary search in a rotated array (ex. {5,6,7,8,1,2,3})
         */
        public static int rotBinSearch(IList<int> arr, int target)
        {
            // unrotate array
            throw new NotImplementedException();
        }

        /**
         * Use dynamic programming to find the first X prime numbers
         */
        public static IList<int> primeNums(int count)
        {
            IList<int> ret = new List<int>();
            for (int i = 1; ret.Count < count; i++)
            {
                if (isPrime(i))
                {
                    ret.Add(i);
                }
            }
            return ret;
        }
        private static bool isPrime(int n)
        {
            if (n <= 1)
            {
                return false;
            }
            for (int i = 2; i*i <= n; i++)
            {
                if (n % i == 0)
                {
                    return false;
                }
            }
            return true;
        }

        /**
         * Write a function that prints out the binary form of an int
         */
        public static string intBinConverter(int n)
        {
            return Convert.ToString(n, 2);
            throw new NotImplementedException();
        }

        /**
         * Implement parseInt
         */
        public static int myParseInt(string str)
        {
            throw new NotImplementedException();
        }

        /**
         * Implement squareroot function
         */
        public static double mySquareRoot(int n)
        {
            throw new NotImplementedException();
        }

        /**
         * Implement an exponent function (bonus: now try in log(n) time)
         */
        public static int myExponent(int n)
        {
            throw new NotImplementedException();
        }

        /**
         * Write a multiply function that multiples 2 integers without using *
         */
        public static int myMult(int a, int b)
        {
            throw new NotImplementedException();
        }

        /**
         * HARD: Given a function rand5() that returns a random int between 0 and 5, implement rand7()
         */
        public static int myRand(Func<int> rand5)
        {
            throw new NotImplementedException();
        }

        /**
         * HARD: Given a 2D array of 1s and 0s, count the number of "islands of 1s" (e.g. groups of connecting 1s)
         */
        public static int oneIslands(IList<int> nums)
        {
            throw new NotImplementedException();
        }
        
    }

    
}
