﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public static class Sorts
    {

        private static void swap(ref IList<int> arr, int from, int to)
        {
            int tmp = arr[to];
            arr[to] = arr[from];
            arr[from] = tmp;
        }

        /** Bubble
         *  - O(n^2)
         *  - compares each pair of adjacent items and swaps them if they are in the wrong order. The pass through 
         *      the list is repeated until no swaps are needed, which indicates that the list is sorted.
         */
        public static IList<int> bubble (IList<int> nums)
        {
            bool switched;
            do
            {
                switched = false;
                for (int i = 0; i < nums.Count-1; i++)
                {
                    if (nums[i] > nums[i+1])
                    {
                        swap(ref nums, i, i + 1);
                        switched = true;
                    }
                }

            } while (switched);
            return nums;
        }

        /** Selection
         *  - O(n^2)
         *  - divides the input list into two parts: the sublist of items already sorted, 
         *      which is built up from left to right at the front (left) of the list, 
         *      and the sublist of items remaining to be sorted that occupy the rest of the list
         *      Proceedes:
         *          1) finding the smallest (or largest, depending on sorting order) element 
         *              in the unsorted sublist, 
         *          2) exchanging (swapping) it with the leftmost unsorted element 
         *              (putting it in sorted order), 
         *          3) and moving the sublist boundaries one element to the right
         */
        public static IList<int> selection(IList<int> nums)
        {
            for (int i = 0; i < nums.Count; i++)
            {
                int iMin = i;
                for (int iUnsorted = i; iUnsorted < nums.Count; iUnsorted++)
                {
                    if (nums[iMin] > nums[iUnsorted])
                    {
                        iMin = iUnsorted;
                    }
                }
                if (i != iMin)
                {
                    swap(ref nums, iMin, i);
                }
            }
            return nums;
        }
        
        /** insertion
         *   - O(n^2)
         *   - Alg:
         *      1) Loop over array
         *      2) set starting point for checks
         *      3) loop backwards while  current-1 > current
         */
        public static IList<int> insertion(IList<int> nums)
        {
            for (int i = 0; i < nums.Count; i++)
            {
                int loop = i;
                while (loop > 0)
                {
                    if (nums[loop-1] > nums[loop])
                    {
                        swap(ref nums, loop, loop - 1);
                    }
                    loop--;
                }
            }
            return nums;
        }
        
        /** merge
         *  - O(n log n)
         *  - stable
         *  - Alg:
                 function merge_sort(list m)
                    // Base case. A list of zero or one elements is sorted, by definition.
                    if length of m ≤ 1 then
                        return m

                    // Recursive case. First, divide the list into equal-sized sublists
                    // consisting of the even and odd-indexed elements.
                    var left := empty list
                    var right := empty list
                    for each x with index i in m do
                        if i is odd then
                            add x to left
                        else
                            add x to right

                    // Recursively sort both sublists.
                    left := merge_sort(left)
                    right := merge_sort(right)

                    // Then merge the now-sorted sublists.
                    return merge(left, right)
         *      
         */
        public static IList<int> merge(IList<int> nums)
        {
            // Base case
            if (nums.Count <= 1)
            {
                return nums;
            }
            // Partition. array into equal halves
            int pivot = nums.Count / 2;
            IList<int> left = nums.Take(pivot).ToList();
            IList<int> right = nums.Skip(pivot).ToList();

            // Recursively partition until we hit the base case of count = 1
            left = merge(left);
            right = merge(right);

            // Merge together the arrays into new array
            IList<int> res = new List<int>();
            // While both arrays aren't empty loop and sort
            while (left.Count > 0 && right.Count > 0)
            {
                // Compare, add to result and remove from array
                if (left[0] < right[0])
                {
                    res.Add(left[0]);
                    left.RemoveAt(0);
                }
                else
                {
                    res.Add(right[0]);
                    right.RemoveAt(0);
                }
            }

            //One may still contain items so loop over
            while (left.Count > 0)
            {
                res.Add(left[0]);
                left.RemoveAt(0);
            }
            while (right.Count > 0)
            {
                res.Add(right[0]);
                right.RemoveAt(0);
            }
            return res;
        }

        /** quicksort
         *   - O(n log n)
         *   - ALG:
         *      1) Pick an element, called a pivot, from the array.
                2) Partitioning: reorder the array so that all elements with values less than 
                    the pivot come before the pivot, while all elements with values greater than 
                    the pivot come after it (equal values can go either way). After this partitioning, 
                    the pivot is in its final position. This is called the partition operation.
                3) Recursively apply the above steps to the sub-array of elements with smaller values 
                    and separately to the sub-array of elements with greater values.
         *   
         */
        public static IList<int> quick(IList<int> nums)
        {
            return quick(nums, 0, nums.Count - 1);
        }
        private static IList<int> quick(IList<int> nums, int low, int high)
        {
            if (low <= high)
            {
                Console.WriteLine("{0} - {1}", low, high);
                int pivot = partition(ref nums, low, high);
                IList<int> left = quick(nums, low, pivot - 1);
                IList<int> right = quick(nums, pivot + 1, high);
            }

            return nums;

        }
        private static int partition(ref IList<int> arr, int low, int high)
        {
            int pivot = arr[high];
            for (int i = low; i < high; i++)
            {
                if (arr[i] < pivot)
                {
                    swap(ref arr, i, low++); //Move element to start of subarray
                }
            }
            swap(ref arr, low, high); //Swap lowest with pivot 
            return low;
        }
    }
}
