﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class NodeList<T> : List<Node<T>>
    {
        public NodeList() : base() { }
        public NodeList(int initSize)
        {
            for (int i = 0; i < initSize; i++)
            {
                base.Add(default(Node<T>));
            }
        }

        public Node<T> findByValue(T value)
        {
            foreach (Node<T> node in this)
            {
                if (node.data.Equals(value))
                {
                    return node;
                }
            }
            return null;
        }
    }
}
